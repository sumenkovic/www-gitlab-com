---
layout: job_page
title: "Engineering Management - Production"
---

The Engineering Manager, Production directly manages the developers and production engineers that run
GitLab.com.

## Engineering Manager, Production

### Hiring Process

Applicants for this position can expect the hiring process to follow the order below. Please keep in mind that applicants can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/team).

* Selected candidates will be invited to schedule a 30 minute [screening call](/handbook/hiring/#screening-call) with our Recruiting team
* Next, candidates will be invited to schedule a 45 minute first interview with our VP of Engineering
* Next, candidates will be invited to schedule a 45 minute second peer interview with either our Director of Security or Director of Quality
* Next, candidates will be invited to schedule a 45 minute third interview with an Engineering team member
* Finally, candidates may be asked to schedule a 50 minute final interview with our CEO
* Successful candidates will subsequently be made an offer via email

Additional details about our process can be found on our [hiring page](/handbook/hiring).

## Director of Engineering, Infrastructure

The Director of Engineering, Infrastructure manages multiple teams that work on GitLab.com and contribute to our on-premise product. They see their teams as their products. They are capable of managing multiple teams and projects at the same time. They are expert recruiters of both developers and managers. But they can also grow the existing talent on their teams. Directors are leaders that model the behaviors we want to see in our teams and hold others accountable when necessary. And they create the collaborative and productive environment in which developers and engineering managers do their work.

### Responsibilities

- Hire and manage multiple infrastructure teams that live our [values](https://about.gitlab.com/handbook/values/)
- Measure and improve the happiness and productivity of the team
- Define the agile development and continuous delivery process
- Drive quarterly OKRs
- Work across sub-departments within engineering
- Write public blog posts and speak at conferences
- Own the quality, security and performance of the product

### Requirements

- 10 years managing multiple software engineering teams
- Experience in a peak performance organization
- Kubernetes, Docker, Go, and Linux administration
- Product company experience
- Startup experience
- Experience with consume scale platforms
- Enterprise software company experience
- Computer science education or equivalent experience
- Passionate about open source and developer tools
- Exquisite communication skills

### Nice-to-have's

- Online community participation
- Remote work experience
- Significant open source contributions

### Hiring Process

Applicants for this position can expect the hiring process to follow the order below. Please keep in mind that applicants can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/team).

* Selected candidates will be invited to schedule a 30 minute [screening call](/handbook/hiring/#screening-call) with our Recruiting team
* Next, candidates will be invited to schedule a 45 minute first interview with our VP of Engineering
* Next, candidates will be invited to schedule a 45 minute second peer interview with either our Director of Security or Director of Quality
* Next, candidates will be invited to schedule a 45 minute third interview with an Engineering team member
* Finally, candidates may be asked to schedule a 50 minute final interview with our CEO
* Successful candidates will subsequently be made an offer via email

Additional details about our process can be found on our [hiring page](/handbook/hiring).
