---
layout: markdown_page
title: "Sales Handbook"
---

## Reaching the Sales Team (internally)

- [**Public Issue Tracker**](https://gitlab.com/gitlab-com/sales/issues/); please use confidential issues for topics that should only be visible to team members at GitLab.
- Please use the 'Email, Slack, and GitLab Groups and Aliases' document for the appropriate alias.
- [**Chat channel**](https://gitlab.slack.com/archives/sales); please use the `#sales` chat channel for questions that don't seem appropriate to use the issue tracker or the internal email address for.

---
## On this page
{:.no_toc}

- TOC
{:toc}

---

## Other Sales Topics in Handbook

* [Sales Onboarding](/handbook/sales-onboarding/)
* [Business Operations](/handbook/business-ops)
* [Sales Skills Best Practices](/handbook/sales-training/)
* [Sales Discovery Questions](/handbook/sales-qualification-questions/)
* [EE Product Qualification Questions](/handbook/EE-Product-Qualification-Questions/)
* [FAQ From Prospects](/handbook/sales-faq-from-prospects/)
* [How to Engage a Solutions Architect](/handbook/customer-success/solutions-architects#when-and-how-to-engage-a-solutions-architect)
* [How to Initiate a Refund](/handbook/finance/Accounts-receivable-and-cash/#sts=Refunds)
* [Client Use Cases](/handbook/use-cases/)
* [POC Template](/handbook/sales/POC/) to be used to help manage a trial/proof of concept with success criteria
* [Account Planning Template for Large/Strategic Accounts](https://docs.google.com/presentation/d/1yQ6W7I30I4gW5Vi-TURIz8ZxnmL88uksCl0u9oyRrew/edit?ts=58b89146#slide=id.g1c9fcf1d5b_0_24)
* [Demo](/handbook/sales/demo/)
* [Dealing with Security Questions From Prospects](/handbook/engineering/security/#security-questionnaires-for-customers)
* [Marketing & Sales Development](/handbook/marketing/marketing-sales-development/sdr/)
* [Who to go to to ask Questions or Give Feedback on a GitLab feature](/handbook/product/#who-to-talk-to-for-what)
* [CEO Preferences when speaking with prospects and customers](/handbook/people-operations/ceo-preferences/#sales-meetings)


## Sales Resources outside of the Sales Handbook

* [Resellers Handbook](/handbook/resellers/)
* [Customer Reference Sheet](https://docs.google.com/a/gitlab.com/spreadsheets/d/1Off9pVkc2krT90TyOEevmr4ZtTEmutMj8dLgCnIbhRs/edit?usp=sharing)
* [Sales Collateral](https://drive.google.com/open?id=0B-ytP5bMib9TaUZQeDRzcE9idVk)
* [Sales Training](https://drive.google.com/open?id=0B41DBToSSIG_WU9LZ0o0ektEd0E)
* [Lead Gen Content Resources](https://about.gitlab.com/resources/)
* [GitLab ROI Calculator](https://about.gitlab.com/roi/?team_size=100&developer_cost=75)
* [GitLab University](https://docs.gitlab.com/ee/university/)
* [GitLab Support Handbook](/handbook/support/)
* [GitLab Hosted](https://about.gitlab.com/gitlab-hosted/)
* [Legal Page](https://about.gitlab.com/handbook/legal/)

## GitLab Tech Stack

For information regarding the tech stack at GitLab, please visit the [Business Operations section of the handbook](/handbook/business-ops#tech-stack) where we maintain a comprehensive table of the tools used across the Marketing, Sales and Customer Success functional groups, in addition to a 'cheat-sheet' for quick reference of who should have access and whom to contact with questions.

## Sales Meetings and Presentations

### Sales Call

* General Guidelines
  1. We start on time and do not wait for people.
  1. If you are unable to attend, just add your name to the list of "Not Attending".
  1. When someone passes the call to you, no need to ask "Can you hear me?" Just begin talking and if you can't be heard, someone will let you know.
* The Sales team call is every Monday 9:00am to 9:30am Pacific Time. The call is cancelled on US holidays that fall on Mondays.
* The agenda can be found here in Google Drive by searching for "Sales Agenda". Remember that everyone is free to add topics to the agenda. Please start with your name and be sure to link to an issue, merge request or commit if that is relevant.
* We use [Zoom](https://zoom.us) for the call since Hangouts is capped at 15 people, link is in the calendar invite, and also listed at the top of the Sales Team Agenda.
* The call is recorded automatically, and all calls are transferred every hour to a Google Drive folder called "GitLab Videos". There is a subfolder called "Sales Team Meeting", which is accessible to all users with a GitLab.com e-mail account.
* The general order of the call is outlined below. Once the presenter has shared all of his/her items, he/she should then ask for questions. If there are no questions or all the questions have been answered, the presenter is to handoff to the next presenter on the agenda.
   1. The CRO will start the call with performance to-date for the month/quarter, then share the remaining forecast for the month/quarter. The CRO will then discuss any number of  topics ranging from pipeline cleanup, process reminders, and other  messages to the group. If the CRO is not attending, the Director of Sales Operations will share this update.
   1. The Director of Sales Operations will provide an update to process changes, new fields, and other updates related to sales process and group productivity.
   1. Marketing will provide an update to Sales. The Sales and Business Development Manager will provide an update on SDR and BDR performance to plan: pipeline created, meetings scheduled, and other SDR/BDR metrics. Other members of marketing may provide updates - new collateral, events, and other marketing-related activities.
   1. Customer Success is next. The Director of Customer Success may provide an update to an existing process or an introduction to a new process. In some cases, Solutions Architects may provide an update as well.
   1. Next, any topics from the group for discussion are encouraged. This can include questions on products, processes, or reminders to the rest of the group. In some cases, a summary of an event (client meetings, meetups, trade shows) will be shared.
   1. The call will conclude with a general 'scrum', which is designed for Account Executives and Account Managers to share anything new they have learned over the last week. This may include some competitive knowledge, new-found strategies or tactics that they have found helpful.
* If you cannot join the call, consider reviewing the recorded call or at minimum read through the sales team agenda and the links from there.

### Sales Brown Bag Training Call

* When: Happens every Thursday at Noon Eastern Time
* Where: https://gitlab.zoom.us/j/354866561

### Sales Functional Group Update

* When: Happens once every 4 to 5 weeks at 11 AM Eastern Time
* Where: Zoom link varies - please be sure to check the calendar invitation
* What: The CRO and Director of Sales Operations will provide the company with an update on sale team performance, notable wins, accomplishments, and strategies planned. You can view previous updates in the "Sales Functional Group Update" on the Google Drive

## Collaboration Applications used by Sales

To ensure everyone can collaborate and contribute, sales uses [Google applications](https://www.google.com/sheets/about/); for example, Google Sheets instead of Excel, Google Docs instead of Word, and Google Slides instead of Keynote or Powerpoint.

You can save your work and find work created by others in our "Google Sales Drive."

## Pitches

### Elevator pitch

**The Problem - Customer Perspective**

Right now every large enterprise is suffering from a lack of consistency:

* Not using the same tools
* Not using the same integrations
* Not using the same configuration
* Not using the same work processes
* Not being judged on the same metrics

And they have processes which block reducing time to value, for example:

* Security reviews that are blocking until approved
* Infrastructure that has to be provisioned
* Fixed release windows
* Production that needs to approve releases
* SOX compliance sign offs that need to happen
* QA cycles
* Separate QA teams
* Separate build teams

**Solution**

GitLab is the only integrated product that can help you reduce time to value.

GitLab is an integrated product for the entire software development lifecycle. It contains chat, issue tracking, kanban boards, version control, continuous integration, release automation, and monitoring. 2/3 of the organizations that self-host git use GitLab, that are more than 100,000 organizations and millions of users.

### Is it similar to GitHub?

GitLab started as an open source alternative to GitHub. Instead of focusing on hosting open source projects we focused on the needs of the enterprise, for example we have 5 authorization levels vs. 2 in GitHub. Now we've expanded the feature set with continuous integration, continuous delivery, and monitoring.

### What is the benefit?

We help organizations go faster to market by reducing the cycle time and simplifying the toolchain development and operations use to push their software to market. Before GitLab you needed 7 tools, it took a lot of effort to [integrate](http://www.thereflex.com/_pdfs/bob_or_fully_integrated_enterprise.pdf) them, and you end up with have different setups for different teams. With GitLab you gain visibility on how long each part of the software development lifecycle is taking and how you can improve it.

### Email Intro

GitLab makes it easier for companies to achieve software excellence so that they can unlock great software for their customers by reducing the cycle time between having an idea and seeing it in production. GitLab does this by having an [integrated product](http://www.thereflex.com/_pdfs/bob_or_fully_integrated_enterprise.pdf) for the entire software development lifecycle. It contains not only issue management, version control and code review but also Continuous Integration, Continuous Delivery, and monitoring. Organizations all around the world, big and small, are using GitLab. In fact, 2/3 of organizations that self-host git use GitLab. That is more than 100,000 organizations and millions of users.

## [GitLab Positioning](/handbook/positioning-faq/)

### Market segmentation

#### Organization size

Our market segmentation for organizations is based on the people in a function that can use an application like GitLab:

1. Strategic: 5000+ IT + TEDD employees
2. Large: 750 - 4999 IT + TEDD employees
3. Mid Market: 100 - 749 IT + TEDD employees
4. SMB: less than 100 IT + TEDD employees (SMB stands for small and medium businesses)

#### Initial owner

The people working with each segment and their quota are:

1. Strategic & Large: Strategic Account Leader (SAL) with a quota of $1.25M incremental annual contract value (IACV) per year
1. Mid market: Account Executive with a quota of $750,000 incremental ACV (IACV) per year
1. SMB: SMB Customer Advocate can close SMB deals directly by referring them to our self-serve web store or assisting them with a direct purchase.

Quotas may be adjusted based on geographic region.

Variable Compensation takes the following into consideration:

* 75% of variable - Incremental ACV
* 5% of variable - Renewal ACV
* 20% of variable - Multi-year prepaid deals and professional services revenue.

#### Quota Ramp

For all quota carrying salespeople, we expect them to be producing at full capacity after 6 full months.  We have the following ramp up schedule and measure performance based on this schedule:

* Month 1 - 0%
* Month 2 - 0%
* Month 3 - 25%
* Month 4 - 50%
* Month 5 - 50%
* Month 6 - 75%
* Month 7+ - 100%

For the purposes of our internal analytics tool, InsightSquared, we have the first 3 months at 25%.  We do this so we do not have overinflated %'s when a salesperson does sell something in months 1 and 2.'

### Customer Success

See our [customer success page in the handbook for more details](/handbook/customer-success/).

### How Sales Segments Are Determined

Sales Segmentation information can be found in the [Business Operations - Database Management](/handbook/business-ops/#segmentation) section.

## GitLab Usage Statistics

Using [GitLab Version Check](/handbook/sales-process/version_check), GitLab usage data is pushed into Salesforce for both CE, EE and EE trial users. Once in Salesforce application, you will see a tab called "Usage Statistics".  Using the drop down view, you can select CE, EE Trails or EE to see all usage data sent to Gitlab.
Since version check is pulling the host name, the lead will be recorded as the host name.  Once you have identified the correct company name, please update the company name. Example: change gitlab.boeing.com to Boeing as the company name.

To view the usage, click the hyperlink under the column called "Usage Statistics".  The link will consist of several numbers and letters like, a3p61012001a002.
You can also access usage statistics on the account object level within Salesforce.  Within the account object, there is a section called "Usage Ping Data", which will contain the same hyperlink as well as a summary of version they are using, active users, historical users, license user count, license utilization % and date data was sent to us.

A few example of how to use Usage Statistics to pull insight and drive action?
* If prospecting into CE users, you can sort by active users to target large CE instances. You can see what they are using within GitLab, like issues, CI build, deploys, merge requests, boards, etc.  You can then target your communications to learn how they are using GitLab internally and educate them on what is possible with EE.
* For current EE users who are below their license utilization, you can engage the customer to understand their plan to rollout GitLab internally and how/where we can help them with adoption.
* For current EE users who are above their license utilization, you can leverage the data to engage the customer.  Celebrate the adoption of GitLab within their organization.  Ask why the adoption took off?  How they are using it (use cases)? Engage them in amending their contract right now for the add-on? Update the renewal opportunity to reflect the increase in usage for the true-up and new renewal amount.
* For current EE users who are not using a EE feature, i.e. CI or issues, you can engage the customer to understand why they are not using it.  Do they know we offer it? Are they using a competitive tool?  Have the integrated their current tool into GitLab? Are they open to learning more about what we offer to replace their current tool?
* For EE trials.  What EE features are they using and not using? If using a EE feature, what are they trying to solve and evaluate? If not using, why and are they open to evaluating that feature?

Take a look at a Training Video to explain in greater detail by searching for the "Sales Training" folder on the Google Drive.

## GitLab CE Instances and CE Active Users on SFDC Accounts

In an effort to make the [Usage/Version ping data](https://docs.gitlab.com/ee/user/admin_area/settings/usage_statistics.html) simpler to use in SFDC, there are 2 fields directly on the account layout - "CE Instances" and "Active CE Users" under the "GitLab/Tech Stack Information" section on the Salesforce account record.

The source of these fields is to use the latest version ping record for and usage record for each host seen in the last 60 days and combine them into a single record. The hosts are separated into domain-based hosts and IP-based hosts. For IP based hosts, we run the IP through a [reverse-DNS lookup](https://en.wikipedia.org/wiki/Reverse_DNS_lookup) to attempt to translate to a domain-based host. If no reverse-DNS lookup (PTR record) is found, we run the IP through a [whois lookup](https://en.wikipedia.org/wiki/WHOIS) and try to extract a company name, for instance, if the IP is part of a company's delegated IP space. We discard all hosts that we are unable to identify because they have a reserved IP address (internal IP space) or are hosted in a cloud platform like GCP, Alibaba Cloud, or AWS as there is no way to identify those organizations through this data. For domain-based hosts, we use the [Clearbit](https://clearbit.com) domain lookup API to identify the company and firmographic data associated with the organization and finally the [Google Geocoding API](https://developers.google.com/maps/documentation/geocoding/start) to parse, clean, and standardize the address data as much as possible.

These stats will be aggregated up and only appear on the Ultimate Parent account in SFDC when there are children (since we don't really know which host goes to which child).

To see a list of accounts by # of CE users or instances, you can use the [CE Users list view](https://na34.salesforce.com/001?fcf=00B61000004XccM) in SFDC. To filter for just your accounts, hit the "edit" button and Filter for "My Accounts". Make sure to save it under a different name so you don't wipe out the original. These fields are also available in SFDC reporting.

A few caveats about this data:
* The hosts are mapped based on organization name, domain, and contact email domains to relate the instances to SFDC accounts as accurately as possible, but we may occasionally create false associations. Let the Data & Analytics team know when you find errors or anomalies.
* Both fields can be blank, but the organization can still be a significant CE user. Both fields being blank just means that there are no instances sending us version or usage pings, not necessarily that there are no active instances.
* Users can be the same person in multiple instances, so if a user exists in several instances at an organization with the usage ping on, they are counted each time they appear in an instance. Users may also be external to an organization, so the number of users may not represent employees and can thus be higher than the number of employees.
* These numbers represent the minimum amount that GitLab is likely being used within the organization, since some instances may have these pings turned on and some off. They also may have just the version ping on, in which case we won't see the number of users. This should not be interpreted as having instances but no users.

For the process of working these accounts, appending contacts from third-party tools, and reaching out, please refer to the [business operations](/handbook/business-ops/) section of the handbook.

## Requesting Assistance/Introductions Into Prospects from our Investors

We have great investors who can and want to help us penetrate accounts.  Each month we send out an investor update and within this update there will be a section called "Asks".  Within this section, we can ask investors for any introductions into strategic accounts. To make a request, within the account object in Salesforce, use the chatter function and type in "I'd like to ask our investors for help within this account".  Please cc the CRO and your manager within the chatter message.  All requests should be made before the 1st of the month so they are included in the upcoming investor update.

If an investor can help, they will contact the CRO, and the CRO will introduce the salesperson and the investor. The salesperson shall ask how the investor wants to be updated on progress and follow up accordingly.

## Parent and Child Accounts

* A Parent Account is the business/organization which owns another business/organization.  Example: The Walt Disney Company is the parent account of Disney-ABC Television Group and Disney.com.
* A Child Account is the organization you may have an opportunity with but is owned by the Parent Account. A Child Account can be a business unit, subsidiary, or a satellite office of the Parent Account.
* You may have a opportunity with the Parent account and a Child Account.  Example: Disney and ESPN may both be customers and have opportunities. However, the very first deal with a Parent Account, whether it is with the Parent Account or Child Account, should be marked as "New Business". All other deals under the Parent Account will fall under Add-On Business, Existing Account - Cross-Sell, or Renewal Business (see Opportunity Types section).
* If the Parent and Child accounts have the same company name, either add the division, department, business unit, or location to the end of the account name. For example, Disney would be the name of the Parent Account, but the Child Account would be called The Walt Disney Company Latin America or The Walt Disney Company, Ltd Japan.
* When selling into a new division (which has their own budget, different mailing address, and decision process) create a new account.  This is the Child Account.  For every child account, you must select the parent account by using the parent account field on the account page. If done properly, the Parent/Child relationship will be displayed in the Account Hierarchy section of the account page.
* Remember that a child account can also be a parent account for another account. For example, Disney-ABC Television Group is the child for The Walt Disney Company, but is the parent for ABC Entertainment Group.
* We want to do this as we can keep each opportunity with each child account separate and easily find all accounts and opportunities tied to a parent account, as well as roll-up all Closed Won business against a Parent Account.

## When to Create an Opportunity

Before a lead is converted or an opportunity is created the following must occur:

1. Authority
  * What role does this person play in the decision process? (i.e. decision maker, influencer, etc.) OR is this someone that has a pain and a clear path to the decision maker
1. Need
  * Identified problem GitLab can solve
1. Required Information
  * Number of potential EE users
  * Current technologies and when the contract is up
  * Current business initiatives
1. Handoff
  * The prospect is willing to schedule another meeting with the salesperson to uncover more information and uncover technical requirements and decision criteria
1. Interest by GitLab salesperson to pursue the opportunity.  Are you interested in investing time in pursuing this opportunity.

### Creating a New Business Opportunity

Since a new opportunity is being created with no previous campaign attribution, we must first associate this opportunity to a lead or contact. This will accomplish two things: first, we will attribute all of the lead or contact's previous campaigns to an opportunity for attribution; and second, the lead or the contact will become the primary contact on the new opportunity.

1. From Lead Conversion: Once you have qualified the prospect via our sales development qualification process (please see the [Sales Development handbook](https://about.gitlab.com/handbook/marketing/marketing-sales-development/) for more information), you will convert the lead.

1. From the Contact Record: On the contact page, click on the New Opportunity button. Then enter all required fields, including Opportunity Name, Stage, and Close Date. Then click Save.

### Creating an Add On Business Opportunity

An add on opportunity will inherit information (including marketing attribution) from the original new business opportunity, so instead of creating an add on opportunity from a converted lead or from a contact record, an add on opportunity is created from the original new business opportunity record. This establishes a parent-child relationship between the original new business opportunity and the add on opportunity you are creating.

1. Go to the original new business opportunity that will become the parent opportunity. For example, if you are selling additional seats to an existing subscription, you should go to the original new business opportunity.
1. Click on the "New Add On Opportunity" button.
1. Update the Opportunity Name (see the [Opportunity Naming Conventions](https://about.gitlab.com/handbook/sales/#opportunity-naming-convention)).
1. Enter the Lead Source, Close Date, and Stage.
1. If there are other fields you'd like to populate you can do so.
1. Click Save.

Once the parent-child opportunity hierarchy is established, as mentioned earlier some information will pass from the parent (new business) to the child (add on) opportunity. This information will be used in our reporting and analysis to track add on business opportunities to their initial sources and team members.

* Initial Lead Source
* Initial Sales Qualified Source
* Initial Business Development Representative
* Initial Sales Development Representative

Please note the addition of some validation rules with the new marketing attribution requirements:

* The parent opportunity must either be a new business or renewal opportunity. A parent opportunity cannot be another add on opportunity.
* All sales-assisted non-portal add on opportunities must have a parent opportunity.

### Tracking Sales Qualified Source in the Opportunity

Sales Qualified Source is dimension used when analyzing pipeline creation, lead conversion, sales cycles, and conversion rates. Sales Qualified Source may be different from the Lead Source of an Opportunity, which captures the original source (event, campaign, etc). For example, if a prospect originates from a trial (lead source), that prospect can be qualified by a BDR, SDR, Account Executive, Channel Partner, or the Web (sales qualified source).

The logic for the Sales Qualified Source is as follows:

1. If the Business Development Representative field (Opportunity) is populated, regardless of opportunity owner, the Sales Qualified Source is "BDR Generated"
2. If the Sales Development Representative field (Opportunity) is populated, regardless of opportunity owner, the Sales Qualified Source is "SDR Generated"
3. If both the Business Development Representative and Sales Development Representative fields are NULL and the opportunity owner is:
   * a Regional Director, Account Executive, or Account Manager, the Sales Qualified Source is "AE Generated"
   * a GitLab team member that is not a Regional Director, Account Executive, or Account Manager, the Sales Qualified Source is "Other"
   * an authorized reseller, the Sales Qualified Source is "Channel Generated"
   * the Sales Admin, the Sales Qualified Source is "Web Direct Generated"

### Reseller Opportunities

Opportunities utilizing a reseller require slightly different data:

* Opportunity Name:
If the partner is an authorized reseller, rename the opportunity with the partner’s nick-name in front, then a dash.  For instance; if it is a Perforce deal, the opportunity name should start with P4 - (whatever your opportunity name is)  This is important for the workflow that solicits updates from the reseller.

* Account Name:
It is important that opportunities using a reseller are created on the END CUSTOMER’s account, and not the reseller’s account.  The account name on an opportunity is never a reseller.  Resellers do not buy licenses; they purchase them on the behalf of an end customer.  For instance, the account name field on an opportunity should never be SHI.

* Opportunity Owner:
Should be the name of the AE who is working the deal with the reseller

* Associating Contact Roles:
After creating the opportunity, click “New” in the contact section to associate contacts with the opportunity.
 - The primary contact should always be a contact at the end user’s account and not a contact at the reseller.  This is important as resellers come and go, and if we do not capture the contact at the end user account, we will not be able to sell to this account if the reseller ends their relationship with us or with the end account.
 - A reseller contact (say, the sales rep at ReleaseTEAM) can, and should be added to the opportunity with the role of Influencer.  NOTE: A contact that works for a reseller should never be added to an end user account.  For instance an employee of SoftwareOne should be a contact of the SoftwareOne account only, and not the Boeing account.

* Associating Partners to an Opportunity:
After creating the opportunity, click “New” in the Partners section to associate the reseller with the opportunity.
 - You can associate multiple partners with an opportunity if there is more than one reseller involved in the opportunity.  This is not uncommon for government opportunities, or opportunities where the customer is asking multiple fulfillment houses (like SHI and SoftwareOne) to fulfill the order.

### Opportunity Naming Convention

Opportunities for subscriptions will use the following guidelines:

- **New Business**: [Quantity]
   - [Name of Company]- [Quantity] [Edition]
   - Example: Acme, Inc- 50 Starter

- **Add-On Business (seats only)**:
   - [Name of Company]- Add [Quantity] [Product]
   - Example: Acme, Inc- Add 25 Starter

- **Add-On Business (Upgrade from Starter to Premium)**:
   - [Name of Company]- Upgrade to Ultimate
   - Example: Acme, Inc- Upgrade to Ultimate

- **Renewal Business (no changes)**:
   - [Name of Company]- [Quantity] [Abbreviations of Product] Renewal [MM/YY]
   - Example: Acme, Inc- 50 Premium Renewal 01/17

- **Renewal Business + Add On Business (seats)**:
   - [Name of Company]- [Quantity] [Product] Renewal [MM/YY]+ Add [Quantity]
   - Example: Acme, Inc- 50 Premium Renewal 01/17 + Add 25

- **Renewal Business + Upgrade**:
   - [Name of Company]- [Quantity] Upgrade to Premium + Renewal [MM/YY]
   - Example: Acme, Inc- 50 Upgrade to Premium + Renewal 01/17

- **Refunds**:
   - [Original Opportunity Name] - REFUND
   - Example: Acme, Inc- 50 Upgrade to Premium + Renewal 01/17 - REFUND

## Opportunity Types

There are three things that can be new or existing:

- Account (organization)
- Subscription (linked to a GitLab instance)
- Amount (dollars paid for the subscription)

That gives 4 types of of opportunities:

1. New account (new account, new subscription, new amount) This type should be used for any new subscription who signs up either through the sales team or via the web portal. Paid training also falls under this type if the organization does not have an enterprise license.
1. New subscription (existing account, new subscription, new amount) If an existing account is purchasing a new license for another GitLab instance, this will be new business.
1. Add-on business (existing account, existing subscription, new amount) This type should be used for any incremental/upsell business sold into an existing subscription division mid term, meaning not at renewal. This may be additional seats for their subscription or an upgrade to their plan. If an existing account is adding a new subscription, this would be new business, not an add-on.
1. Renewal (existing subscription, existing subscription, existing amount) This type should be used for an existing account renewing their license with GitLab. Renewals can have their value increased, decreased, or stay the same.  We capture incremental annual contract value growth/loss as a field in Salesforce.com. Renewal business can be a negative amount if renewed at less than the previous dollars paid for the subscription (renewal rate). Only the part that is more or less than the old amount is IACV, the rest is part of the the renewal opportunity.

**New business** is the combination of new account and new subscription.

## Deal Sizes

Deal Size is a dimension by which we will measure stage to stage conversions and stage cycle times of opportunities. Values are [IACV](https://about.gitlab.com/handbook/finance/operating-metrics/#bookings-incremental-annual-contract-value-iacv) in USD.

1. Jumbo - USD 100,000 and up
1. Big - USD 25,000 to 99,999.99
1. Medium - USD 5,000 to 24,999.99
1. Small - below USD 5,000

## Capturing "MEDDPIC" Questions for Deeper Qualification

MEDDPIC is a proven sales methodology used for complex sales process into enterprise organizations. These questions should be answered in the "1-Discovery" Stage of the sales process. These questions will appear in the MEDDPIC Section of the Opportunity layout and will be required for all new business opportunities that are greater than $25,000 IACV.

* (M) Metrics: What is the economic impact of the solution?
  * Top line metrics include quicker time to market, higher quality- improvements towards sales and revenue.
  * Bottom line metrics include reduction in operating costs.
* (E) Economic Buyer: Who has profit/loss responsibility for this solution?
  * Economic Buyer (EB) has the power to spend.
  * Check with the EB on sponsorship, selection criteria.
* (D) Decision Criteria: Understand what is driving the decision?
  * What is the Technical Decision Criteria (TDC)?
  * What is the Business Decision Criteria (BDC)?
  * Check with both the Champion and the EB.
* (D) Decision Process: What will the decision-making process look like? Who is involved? What is their criteria for selection?
  *  What is the Technical Decision Process (TDM)?
  *  What is the Business Process (TBM)?
* (P) Purchasing Process: What does the purchasing process entail?
  * Is there a legal team involved in contract negotiations?
  * What does a purchase of this size look like?
  * Has a purchase of this size happened before?
* (I) Identify Pain: What are the primary objectives?
  * Is there a compelling event?
  * Remember, nobody buys until there is pain.
* (C) Champion: Who will sell on your behalf inside the company?
  * Is this person qualified to be your Champion?
  * Have you tested this person as your Champion?
  * Does this person know he/she is your Champion?

## Tracking Proof of Concepts (POCs)

In Stage 3-Technical Evaluation, a prospect or customer may engage in a proof of concept as part of the evaluation. If they decide to engage, please fill in the following information, as we will use this to track existing opportunities currently engaged in a POC, as well as win rates for customers who have engaged in a POC vs. those that do not.

1. In the opportunity, go to the 3-Technical Evaluation Section.
1. In the POC Status field, you can enter the status of the POC. The options are Not Started, Planning, In Progress, Completed, or Cancelled.
1. Enter the POC Start and End Dates.
1. Enter any POC Notes.
1. Enter the POC Success Criteria, ie how does the prospect/customer plan to determine whether the POC was successful or not.

## Capturing Purchasing Plans

Once you have reached Stage 4-Proposal, you should agree to a purchasing plan with your prospect/contact. This will include a clear understanding of purchase/contract review process and a close plan. This should include actions to be taken, named of people to complete actions and dates for each action. The information captured in this field will be used by sales management to see if the deal is progressing as planned. Delays in the purchasing plan may result in a request to push the opportunity into the following period.

### Submitting Weekly Forecasts

Each week, SALs, AEs, AMs, and Channel Managers are responsible for submitting a weekly forecast for the current quarter in InsightSquared. Regional forecasts are due by **Monday 10 AM Pacific Time**. The regional numbers will be documented and shared on the weekly forecast call.

There are two categories that opportunities will fall into when submitted for a forecast:

* **Commit**: refers to opportunities that are either currently in `5-Negotiating` stage or in an earlier stages with a high probability (90%) of closing in the period. You should be very confident that these deals will close in this period.
* **Best Case**: refers to opportunities that are either currently in `4-Proposal` stage or in an earlier stage with a moderate (50%) probability of closing in the period. You do not have to be as accurate as the Commit forecast, but you should still practice discretion as you add opportunities to Best Case forecast.

Please use these terms correctly and don't introduce other words. Apart from the above the company uses two other terms:

* **Plan**: Our yearly operating plan that is our promise to the board. The IACV number has a 50% of being too low and 50% chance of being too high but we should always hit the TCV - Opex number.
* **Forecast**: Our latest estimate that has a 50% of being too low and 50% chance of being too high.

To submit a forecast, complete the following steps:

1. Log into InsightSquared and go to `Settings` on the top right.
1. Select `Forecast Entry`.
1. Go to the month you'd like to forecast.
1. Click on the magnifying glass icon next to the `Commit` field.
1. Select the opportunities you'd like to add to your commit for that month. You can include opportunities up to three (3) months out.
1. As you select opportunities, you will see the total value field increase. Once you're done, click on the `Use This Number` field.
1. Click on the magnifying glass icon next to the `Best Case` field for that same month.
1. You will have to re-select the opportunities you selected in the `Commit`, then add additional opportunities you feel have a chance to close in the selected period. As a result your Best Case should never be less than your Commit.
1. Repeat this for all of the months available (three months).

If you are a Regional Director or oversee a team of sales reps, you can overwrite your entire team's forecast or that of an individual team member:

1. Log into InsightSquared and go to `Settings` on the top right.
1. Select `Forecast Entry`.
1. If you want to overwrite your entire team's forecast, go to the very top line of the forecast hierarchy and enter the value that you believe will close. The box should include your title or role (eg, Regional Director- EMEA).
1. If you want to override an individual team member's forecast, go to that team member's line in the forecast hierarchy and enter the value you believe will close.
1. Enter the Commit and Best Case for each of the months that you would like to override.
1. Click `Save`.

The final forecast will appear in the [Regional Director Forecast by Team](https://clients.insightsquared.com/reports/manual_forecast/by_team/?sort=name%7C-forecast%7Cgoals&is_expanded=MTIzLDE0MCw4xII5Miw5OMSDMTbEjzTEj8SOxIDEhjnEkTEwxJE4xI44xJPEgMSLxJrEoDDEhsSaxI45xIY4xJk0McSDNMSiNMSVNcSCxK0sxLDEtzksNcSGN8SexKIsNyzEizPEjjTFgsSy&ignore_subgoals=0&legendchecked=UGlwZWxpbmUsQm9va8SFZ3MgR29hbA%3D%3D&date_tab=this_quarter&legendunchecked=UGlwZWxpbmU%3D&is_checked=OTM%3D&second_date=monthly_view&logic_selector=OR&f_type=1) report in the [Sales Forecast](https://clients.insightsquared.com/reports/dashboard/db117617/) dashboard in InsightSquared.

Here is a [link](https://insightsquared.zoom.us/recording/play/pbXvPolz72rULs-GZazwgYqEaLu4IBhnGWg2wD2pMygiY8ZmzPF8jYb3j3C7Q-wr) to a recorded training session with InsightSquared.

## Associating Emails to Opportunities

Associating emails to Salesforce is a best practice that should be followed by all users. Attaching emails to a Lead, Account, Contact, or Opportunity record are vital for transparency, as any number of team members can review the correspondence between GitLab and the customer/prospect account. It doesn't do anyone any good when all of the correspondence is kept in a user's inbox.

With that said, there are multiple ways that an email can be associated to an opportunity. But before you do that, you must first make sure that the contact record is associated to an opportunity via the Contact Roles object.

### First, Contact Roles

To add a Contact Role to an opportunity:
1. Go to the Opportunity.
1. Go to the Contact Role related list.
1. Click 'New'.
1. Add the Contact.
1. If this person is your primary contact, click on the 'Primary' radio button.
1. Add the Role this person plays in the opportunity (Economic Buyer, Technical Buyer, etc).
1. Repeat the steps, although you can only have one Primary Contact per opportunity.
1. For more information, you can visit the [Salesforce Knowledge Center](https://help.salesforce.com/articleView?id=contactroles_add_cex.htm&type=5).

### Email to Salesforce

Email to Salesforce allows you to associate emails to Opportunities using an email long email string that associates the email to the contact. To set this up:

1. Click on your 'Name' on the top right of any page in Salesforce.
1. Select 'My Settings'
1. Go to the left sidebar and click 'Email'.
1. Then click 'My Email to Salesforce'.
1. Copy the email string.
1. Go to My Acceptable Email Addresses and make sure your gitlab email address is there.
1. In 'Email Associations', make sure "Automatically assign them to Salesforce records" is checked.
1. Check Opportunities, Leads, and Contacts.
1. In the Leads and Contacts section, make sure to select "All Records".
1. It is up to you if you want to save all attachments, as well as if you want to receive an email confirmation of an association (my recommendation is yes for the first few weeks to make sure it's working, then you can turn it off anytime).
1. Click Save.
1. Go to Gmail and save this email address in a Contact record. A good practice is name it 'BCC SFDC' for first and last name.
1. When you send any emails from Gmail, you will BCC the "BCC SFDC" contact, which will send the email to long string you copied in Step 5.

### Outreach

If you want to associate emails to Opportunities using Outreach, follow these steps:

1. Go to your photo on the bottom left.
1. Click Settings
1. Select 'Plugins' on the left menu.
1. Select the SFDC (Salesforce) plugin.
1. Click on 'Contact'.
1. On the bottom right, enable 'Automatically associate activity with Opportunity'
1. Click the 'Save' button on the top right.

### Salesforce Lightning for Gmail

If you want to associate emails to Opportunities or other records using the the Salesforce Lightning for Gmail plug in, follow these steps:

1. Visit the Chrome Store to download the [Salesforce Lightning for Gmail](https://chrome.google.com/webstore/detail/salesforce-lightning-for/jjghhkepijgakdammjldcbnjehfkfmha) plug in.
1. Click `Add to Chrome`
2. Click `Add Extension`
2. Go to Gmail and open the right sidebar.
3. When you open an email that may contain email addresses from existing leads or contacts in Salesforce, all related records associated to that email (Lead, Contact, Account, Opportunity, and Cases) will appear, and you can select any or all records to related the email to.
3. For each record you'd like to associate the email to, click on the upload icon for each record.
4. If you wish, you can include some or all of the attachments to the record as well by clicking the checkbox next to each attachment.

If you have any issues with the setup, please contact the Director of Sales Operations via Slack, email, or SFDC Chatter.

## Escalation to Support

During the sales cycle potential customers that have questions that are not within the scope of sales can have their queries escalated in different ways depending on the account size:

1. For Large accounts that will have a dedicated Solutions Architect, [engage the SA](/handbook/customer-success/solutions-architects#when-and-how-to-engage-a-solutions-architect) so that they can triage and develop the request.
1. For questions that you think technical staff can answer in less than 10 minutes, see the [internal support](/handbook/support/#internal-support-for-gitlab-team-members) section of the support handbook.
1. If a potential customer has already asked you a question, forward a customer question via email to the **support-trials** email address. - It's important the email is **forwarded** and not CC'd to avoid additional changes required once the support request is lodged.

### How Support Level in Salesforce is Established

Once a prospect becomes a customer, depending on the product purchased, they will be assigned a Support Level (Account) in Salesforce:

#### GitLab EE
1. Premium: Ultimate customers; Premium customers; any Starter customer who has purchased the Premium Support add on
1. Basic: any Starter customer without the Premium Support add on
1. Custom: any customer on Standard or Plus legacy package

#### GitLab.com
1. Gold: Gitlab.com Gold Plan
1. Silver: Gitlab.com Silver Plan
1. Bronze: Gitlab.com Bronze Plan

If a customer does not renew their plan, their account will be transitioned to an "Expired" status.

## Contributing to EE Direction

Being in a customer facing role, salespeople are expected to contribute to [GitLab Direction](https://about.gitlab.com/direction/). Each day we talk to customers and prospects we have the opportunity to hear of a feature they find valuable or to give feedback (positive and constructive) to an EE feature that there is an issue for.
When you hear of feedback or you personally have feedback, we encourage you to comment within the issue, if one exists, or create your own issue on our [EE Issue Tracker](https://gitlab.com/gitlab-org/gitlab-ee/issues). Checking the [GitLab Direction](https://about.gitlab.com/direction/) page and the [EE Issue Tracker](https://gitlab.com/gitlab-org/gitlab-ee/issues) should happen throughout the week.

When you have an organization that is interested in an EE feature and you have commented in the issue and added a link to the account in salesforce, please follow the process outlined on the [product handbook](/handbook/product/#example-a-customer-has-a-feature-request) to arrange a call with the product manager and account to further discuss need of feature request.

## Export Control Classification, and Countries We Do Not Do Business In

GitLab's Export Control Classification (or ECCN) is 5D002.c.1 with CCATS number G163509. This means that GitLab source code can be exported and re-exported under the authority of license exception TSU of section [740.13(e)](https://www.bis.doc.gov/index.php/forms-documents/doc_view/986-740) of the export administration regulations (EAR).

Per [740.13(e)(2)(ii)](https://www.bis.doc.gov/index.php/forms-documents/doc_view/986-740) of the EAR, there are restrictions on "Any knowing export or reexport
to a country listed in [Country Group E:1 in Supplement No. 1 to part 740 of the EAR](https://www.bis.doc.gov/index.php/forms-documents/doc_download/944-740-supp-1)".

As a consequence of this classification, we currently do not do business in Iran, Sudan (excluding South Sudan), Syria, North Korea, and Cuba.

## Sales Order Processing

### Creating Accounts, Contacts, Opportunities, and Quotes in Salesforce

#### Step 1 - Create an Account

1. Click on the Accounts tab
1. Click on New button to Create Account
1. Select either Standard or Channel record Type. Each record type will have a different Account layout based on our engagement.
1. Standard Record Type should be used for all non-reseller partner accounts.
1. Channel Record Type should be used for all reseller partner accounts.
1. Create Account Screen – Enter all mandatory fields and click on Save – Account is created

#### Step 2 - Create a Contact

1. Create Contacts, by clicking on the new contact button
1. Make sure to add in the address for any contacts associated with the quote (Bill To and Sold To)

#### Step 3 -  Create an Opportunity

1. Please see the [Creating a New Business Opportunity](#when-to-create-an-opportunity) section for an overview on how to create opportunities based on new vs. growth.
1. Select the Opportunity Record Type. Like Accounts, the options are Standard and Channel.
   * Standard should be selected when engaged in a Sales opportunity.
   * Channel should be selected when engaged in a Reseller opportunity (attempting to sign a reseller partner in a new territory, not Sales opportunities where resellers are involved).
1. Enter all the required fields based on your business process and click on Save Button

#### Step 4 - ZQuotes – New QUOTE

This is the process to create a quote. If you want to clone an existing quote, please see the Cloning an Existing Quote section.

1. Click on New Quote button within the opportunity
1. Enter all details and click on Next and select products to complete the Quote creation process
1. All the fields marked in red are mandatory fields, below are fields description
1. Valid Date – Date until when Quote is valid; This is auto set to 30 days from today’s date
1. Quote Template – click on the button to select the Quote template
1. Bill To contact  - Click on the vlookup button to enter the Bill To contact information. This will bring up a pop-up window that will list all the contact that were created during Step 2 of the process. Bill To person would be the contact to whom the bill will be sent
1. Sold To Contact - Click on the vlookup button to enter the Sold To contact information. This will bring up a pop-up window that will list all the contact that were created during Step 2 of the process. Sold To person would be the contact to whom the product was sold. For GitLab.com deals, please make sure that the Sold To Email is the same as the email used on the GitLab.com account.
1, Invoice Owner -
* This field will only be used incase of creating a Quote created for a End Customer that involves a Partner; Please see [Creating a Quote for Partner Section](#resellerQuote)
1. Invoice Owner Contact - is the person at the Invoice Owner who will receive the invoice.
1. Note that the GitLab entity information will be populated via the following rules:
 * If a Direct Customer or Invoice Owner is an Unauthorized Reseller and the Ship To Country is the United Kingdom, the entity is GitLab Ltd (UK).
 * If a Direct Customer or Invoice Owner is an Unauthorized Reseller and the Ship To Country is the Netherlands, the entity is GitLab BV (NL).
 * If a Direct Customer or Invoice Owner is an Unauthorized Reseller and the Ship To Country is not the United Kingdom or the Netherlands, the entity is GitLab Inc. (US).
 * If the Invoice Owner is an Authorized Reseller and the Ship To Country is the United Kingdom, the entity is GitLab Ltd (UK).
 * If the Invoice Owner is an Authorized Reseller and the Ship To Country is the United States, the entity is GitLab Inc. (US).
 * If the Invoice Owner is an Authorized Reseller and the Ship To Country is not the United Kingdom or the United States, the entity is GitLab BV (NL).
1. Payment Method -  refers to the type of payment the customer is using for paying this Quote/Subscription
1. Payment Methods currently defined are as follows –
 * Credit Card
 * ACH
 * Check
 * Wire Transfer
1. Currency - only USD is available.
1. Click Thru EULA - is used when an agreement has not been signed; A use case being a product (EE) is sold through a partner
1. Start Date -  Specify the date on which this subscription, or contract, is effective. This date becomes the Contract Effective Date of the subscription in Zuora. Note: Customers can purchase in advance of the subscription Start Date. In this case, when the Quote is pushed to Z-billing the license generated will be encrypted with the future Start Date and will not function until then.
1. Subscription Term Type:
 * By default set as Termed
1. Initial Term and Renewal Term -  Specify initial term and the renewal term in months
1. Auto Renew -  it is checked by Default; Subscription automatically renew when the initial term for a subscription is over. This is turned off when the Invoice Owner is a Reseller.
1. Tax Exempt - Need to check with client to make sure they are tax exempt and load tax exempt certificate into their account in SFDC.
1. Tax Exempt - If tax exempt click yes on drop down menu and add additional notes if needed.
1. If the Invoice will come from GitLab Ltd or GitLab BV, you are required to enter the VAT/Tax ID field. A validation error will not allow you to save the quote, so if you do not have this information available, just enter "0000" until you receive it. You will not be able to send a quote to Zuora if the VAT/Tax ID is "0000".
1. Special Terms and Notes - Enter any additional notes that is not specified by the above settings.
1. Click on Next to make the product selection
1. Product Selector Page
1. Click on Product Vlookup Button to make the product selection
1. Click on Rate plan Vlookup Button to make the rate plan selection
 * GitLab Enterprise Edition is the rate plan when selling per license seats @ $39
1. In the Discount field, enter the discount in percentage (%) you wish to apply. This will automatically update the Effective Price field.
1. Conversely, if you wish to apply a specific price to a product, enter the price in the Effective Price field. This will update the Discount (%) field.
2. Enter the quantity of the product.
1. After you hit enter, the Total Price of the line item will update.
1. Finally, enter the Period of the line item.
1. Click Save to save the changes you made to the line item.
1. If you need to add additional products to the quote, click New Product Button and repeat the steps of adding a product, rate plan, discount, price, and quantity.
1. After you've completed adding all products to a quote, click Next Button and you will be taken back to the Quote record detail screen.
1. Once the quote has been created, you can modify it, delete it, or send it from Salesforce to Zuora to create a subscription.
1. Before you can deliver the quote to the prospect or customer, it must be approved. Please refer to the [Opportunity Discounting Approval Matrix](https://docs.google.com/document/d/1-8TG8YLAQB-465mFLYnX3wkB6C6-0aI1A4CzdfjpacU/edit) to review the approval tiers.
1. To request approval, go to the Opportunity record in Salesforce and go to the Chatter feed. To direct the Chatter message to a particular person or persons, use `@firstnamelastname`. Note that multiple people may be required to approve a quote (eg, Discount for EE Premium greater than 10% and an ACV greater than $499,999.99). A Chatter message will ensure all requests and approvals are documents within the opportunity.

#### Step 5 – Generate Quote

1. Once a quote has been approved, go back to the quote and click Generate PDF or Generate Word.  The document will be saved as an attachment in the Notes and Attachments section in the opportunity record.
2. The default template for all transaction types (new, amendment, or renewals) will be the direct (non-channel) quotes that do not show $0 line items. If you want to select a different template, click the search icon next to the Quote Template field and select the desired template. A description of each template will be visible next to each template.

#### Step 6 – Send for signature via Sertifi

1. Within the ZQuote object, click Sertifi E-Sign button near the top of page.
1. 1st signer will be auto-populated with the bill to contact.  You can change if needed by clicking on the search icon next to the contact name field.
1. You will now be asked to attach the Quote PDF.  Under the drop down for Related Notes and Attachments.  Select the quote for signature and click send for signature.
1. Once the prospect/customer has signed, you will receive an email with the signed quote as an attachment. The signed document will also be added to the Notes and Attachments section of the Opportunity.

#### Step 7 – Send to Zuora

1. Once the Quote is signed, the Sales rep will click on Send to Zuora button. This will complete a few actions. First it will create the subscription in Zuora. Second, it will create the Billing Account. Lastly, it will prepare the system to send an invoice to the customer (this is completed by the Finance team).
2. To send a quote to Zuora, go to the Quote and click on the Send to Zuora button.
3. Review the customer and product details.
4. Click Submit.

##### Quote Metrics

The following quote metrics are available:

 * MRR (Monthly Recurring Revenue) - Monthly recurring revenue (MRR) calculates subscription recurring fees normalized to a monthly value.
 * Delta MRR - Delta of the total MRRs between the original subscription and the amendment.
 * TCV (Total Contract Value) - Total contract value (TCV) calculates the total recurring charges over the lifetime of a subscription.
 * Delta TCV - Delta of the TCVs between the original subscription and the amendment.
 * Sub-total - The quote subtotal for the specified billing periods, excluding discount charges and taxes.
 * Discount - The sum of all discount charges for the specified billing periods.
 * Tax - The tax calculated for the specified billing periods.
 * Total - The quote total for the specified billing periods, including discount charges and taxes.

##### Edit Quote Button

 * You can only edit a quote before it is sent to Z-Billing. After a quote is sent to Z-Billing and its subscription is created, you can no longer edit the quote in Salesforce.

##### Edit a Quote

 * To Edit a Quote, click Edit Quote Details.
 * On Edit Quote Details page, make the desired changes.
 * Click Save.

##### Select products Button

 * To add, remove, or update products, rate plans, and charges for a quote sales rep can click Select Products.
 * Make necessary changes to products, rate plans, and charges
 * Click Save.

##### Delete Button

 * On the Quote Detail page, click Delete Quote Details to delete a quote that was created.
 * A message would appear to confirm on delete on click of ok quote gets deleted

##### Generate PDF Button

 * This will allow the sales rep to Generate a quote pdf
 * On click of Generate PDF – Quote PDF gets generated

#####  Send to Z-Billing Button

 * On the Order Preview page that opens, review the information, and then click Submit to send the quote to Z-Billing.
 * A confirmation popup shows up, Zuora Quotes has successfully sent your quote to the Z-Billing and a subscription was created

#### Step 8 – Before You Submit an Opportunity for Approval

The following is a list of required documents/processes needed before you submit an opportunity for approval:

#####  Direct Deals that do not involve resellers
1. Prospect/Client paid via Credit Card through the web portal (terms are agreed upon sign up).
2. Prospect/Client has returned a signed quote (attach to the opportunity). Quote required for all purchases not made via web portal in order to confirm products purchased, # of seats, term, and pricing.  Quote is also needed to confirm they agree to terms and conditions.
3. In rare circumstances a PO can be accepted in lieu of a signed agreement along with customer acceptance of the click through EULA.  The PO must be submitted to legal@gitlab.com in advance for approval.  Note that the vast majority of customer POs carry terms that invalidate the EULA.

#####  Authorized Reseller Deals that involve an official reseller purchasing on behalf of an End User
1. Reseller has returned a signed authorized reseller quote (attach to opportunity). Quote required for all purchases in order to confirm products purchased, # of seats, term, and pricing.  Quote is also needed to confirm they agree to terms and conditions.
2. Clickthrough EULA must be delivered and accepted by the End User. Please attach a Note to the Notes and Attachments section with a confirmation link or email.

#####  Unauthorized Reseller Deals that involve an unofficial reseller purchasing on behalf of an End User
1. If a PO is received, it must reference our quote showing the Quote ID, products, # of users, term, and pricing of the subscription.  The acceptance of terms language can be removed but click-thru EULA needs to be checked when sending out the license key unless the customer has previously accepted a EULA.
2. If the customer has previously accepted a EULA, then we can use it as the governing terms for this purchase.  In such a case find the ```EULA Acceptance ID``` from our [EULA Request Server](https://customers.gitlab.com/admin/eula_request) and ```EULA Acceptance Date``` and insert the following into the quote: *"By accepting this quote, you, and the entity that you represent (collectively, “Customer”) unconditionally agree to be bound by the terms agreed to in EULA ```EULA Acceptance ID``` previously accepted on ```EULA Acceptance Date```."*
1. If the PO is **approved** by legal, the reseller must return an unauthorized reseller quote (no signature) and is attached to the opportunity. Quote required for all purchases not made via web portal in order to confirm products purchased, # of seats, term, and pricing.  Quote is also needed to confirm they agree to terms and conditions. Also, a Click through EULA must be delivered and accepted by the End User. Please attach a Note to the Notes and Attachments section with a confirmation link or email.
1. If the PO is **not approved** by legal, the end user must sign a standard quote between GitLab and the end user company. Please follow the steps in the Direct Deals section above.

#####  Required fields in Salesforce before submitting for approval
Once you've gathered all required documents and have uploaded them into the Notes and Attachments section of the Opportunity, you'll need to make sure certain fields are populated in the Account and Opportunity records in Salesforce.

1. On the Account record:
  * Industry
  * Billing and Shipping Address
  * In the Special Terms field, add any non-standard terms related to the subscription (ramps, special pricing), support (non-standard SLAs), finance (special billing/payment terms), or legal.
1. On the Opportunity record:
  * Attach any signed agreements, POs, and quotes as an attachment to the opportunity record in Salesforce.com.  If sent/signed via Sertifi, this will happen automatically.
  * Go to the Contact Roles related list and add a Primary Contact. Ideally, you'll add Contact Roles much earlier in the sales cycle.
  * If the opportunity involved a partner, meaning partner will get credit for the opportunity, please click new in the partner section and add partner. There should only be one partner selected for each opportunity.
  * Add the Competitors. Note that this is only required for New Business; it is not required for Renewals or Add On Business.
  * Make sure your Close Date is for the date you are submitting the opportunity for approval.
  * Provide the Closed Won Details- a few sentences on highlighting why we won the deal (pricing, packages, feature set, etc.) should do.
  * If a EULA is delivered to an end-customer, you are required to add the EULA link from the EULA Request Server into the opportunity in Salesforce. Again, the EULA must be accepted by the end customer before you submit the opportunity for approval, but you are required to provide the link, nonetheless.
  * Once these steps are completed, save the record and submit the opportunity for approval.

#### Step 9 – Submitting an Opportunity for Approval

1. Submit the Opportunity for Approval:
  * Click on the Submit for Approval button on the opportunity. The button is along the top of the page before the Opportunity Details section.
  * The opportunity page will then be in "edit" mode. Click on Save. This allows Salesforce to check for certain validation errors before the submission process is completed.
  * If you have any validation errors, please resolve those before clicking Save. Once you have resolved the issues, click Save.
  * If you run into an error submitting the opportunity for approval:
       1. Check the Close Date. The Close Date should be equal or greater than 2016-12-12, the day the approval process was implemented.
       1. Make sure the Approval Status is not equal to "Approved". If the opportunity was already approved, there is no need to resubmit for approval.
  * Once you submit the opportunity for approval, it will be locked, meaning that you will not be able to make any updates. If you'd like to unlock the opportunity to make any changes, you'll have to recall the approval submission. Scroll down to the Approval History section and click on the Recall Approval Request button. Once you've made your changes, you can resubmit your opportunity for approval.
  * An email will be sent to you confirming that the opportunity has been submitted for approval.
  * An email will also be sent to the approval queue, which consists of members of Finance and Sales Operations.
  * If the opportunity has been rejected, the approver will add notes in the Approval Notes field. You will then receive an email explaining why the opportunity was rejected. Please resolve the issues, then resubmit the opportunity for approval.
  * The Stage will be reverted back to Verbal Commitment, regardless of the previous Stage when initially submitted.
  * If the opportunity has been approved, you will receive an email that the opportunity has been approved.
  * The opportunity will automatically change to Closed Won and the Close Date will update to the date of submission.

### After the Deal is Closed Won

Once an opportunity is approved and marked as Closed Won, the following occurs:

1. After an opportunity is Closed Won, it is locked. You will not be able to change any fields on the opportunity object. If changes are necessary, please Chatter Francis or GitLab AR on the opportunity record in Salesforce with the field names and values that need updating. Once the changes are made, you will receive a response via Chatter. Note that you can still add notes and attachments to the record, but other changes that affect the values of the opportunity will result in a validation rule error.
1. Depending on the Sales Segmentation, the account and/or opportunity may be assigned to a new team member:
  * If Sales Segmentation is SMB, then the renewal opportunity will be assigned to Sales Admin. The account will stay with the current Account Owner, but can be reassigned to Sales Admin or other user if desired.
  * If Sales Segmentation is Mid-Market, then the renewal opportunity will be assigned to the Account Manager by Region. The account will also be reassigned to the Account Manager by Region.
  * If Sales Segmentation is Large or Strategic, then the renewal opportunity will be assigned to the current Account Owner. The account will also remain with the current Account Owner, but can be reassigned if desired.
  * Please note the following exceptions to these assignments, regardless of Sales Segmentation:
    * If the Account Manager field is populated, the renewal opportunity and account will be reassigned to this user.
    * If the Account Owner is a Partner User, the renewal opportunity and account will remain with the Partner.
    * If the Customer is a Federal government account, the renewal opportunity and account will remain with the current Account Owner.
1. If the customer agrees to be a reference, please complete the following steps:
  * In the Referenceable Customer field on the account page, change the picklist value to "Yes".
  * Select all the Reference Types they are willing to offer (please see the next section for an explanation of the Reference Types).
  * Enter any Reference Comments related to the customer's willingness to be a reference.
  * Also go to the contact object who agreed to be a reference and under the field "role" please select "reference - investors and prospects".
  * If customer agrees to speak with product marketing about how they use GitLab, please email product marketing manager.
1. If the customer declines to be a reference in any way, please note that we cannot mention them in any external conversations with prospects or investors. Please make sure to add notes in the Reference Notes field on why the customer declined.
1. Once the opportunity is closed won, the field "type" on the account object will change to "Customer".
1. Create an add-on or Existing Account (new division) opportunity if there has been one identified by you at this time.

**Reference Types:**

* Homepage: The customer allows GitLab to use their logo on the GitLab homepage. Please obtain an image file with their logo, or gain customer acceptance of a logo to be used on the GitLab website.
* Customer Story: The customer allows GitLab to share their story and use case with prospects and investors.
* Case Study: The customer allows GitLab's marketing team to draft content highlighting their business challenges and how GitLab solved those challenges.
* Verbal Reference: The customer agrees to speak with either investors or prospective customers on their experience with GitLab.

#### Cloning an Existing Quote

If you'd like to save time by cloning an existing quote, you can do so by doing the following:

1. On the Quote Detail page of the quote you want to clone, click Clone Quote.
2. On the Quote Clone Configuration page, select the following options:
    * Clone Products: Select to clone the products associated with the quote. This option only applies to the New Subscription quotes. Ensure that the products associated with the quote are maintained in the current version of the product catalog.
    * Maintain Quote: Select to be directed to the first step in the Quote Wizard flow that allows you to edit the newly cloned quote. The flows are configured based on the quote type, i.e., New Subscription, Amendment, and Renewal, in the Quote Wizard Settings.
5. Click Next.
    * If you selected the Maintain Quote option, you are redirected to the first step in the Quote Wizard of the newly cloned quote.
    * If you did not select the Maintain Quote option, you are redirected to the Quote Detail page of the newly cloned quote.
8. Please note that you cannot clone the products on an amendment (add-on or renewal quote.)

#### Using the Primary Quote Checkbox

If you create multiple quotes for a single opportunity (EEP vs. EES, single year vs. multi-year, etc.), you can select one of the quotes as the "primary", which  will push the details of that quote to the opportunity. Once you select a different quote as primary, the details of the opportunity are overwritten with the new primary quote's details (amount, initial term, start date, and related quote).

#### Opportunity Values

The following fields are populated via a series of workflows or calculations from an associated quote or subscription in Zuora to the Opportunity object in Salesforce. For more information on some of the following values and how they are defined, please see the [GitLab Metrics](https://about.gitlab.com/handbook/finance/operating-metrics/) of the handbook.

1. Annual Contract Value (ACV) is the value of first twelve months of a subscription. This field is calculated by the Opportunity Term and the MRR fields on the Opportunity. Here are the calculations: If the Opportunity Term is greater than 12 months, the ACV will be the `12 * MRR`; otherwise the ACV will be `Opportunity Term * MRR`.
2. Renewal ACV is manually populated at this time and takes all renewing opportunities and sums the ACV of these opportunities. For example, there are two opportunities (a new opportunity at 7,800 ACV and an add-on at 3,900 ACV). The Renewal ACV for the renewal opportunity will be 11,700.
3. Incremental ACV is `ACV - Renewal ACV`. This value can be negative if the customer is planning to renew at a lower ACV than the previous subscription.
4. Opportunity Term is a value that is captured based on certain scenarios:
   * Single Quote
      * For New Business with a single quote, this field will be populated via the `Initial Term` field on the Quote object.
      * For Renewals with a single quote, this value will be populated via the `Renewal Term` field on the Quote object.
      * For Add-On Business with a single quote, this value will be populated via a formula that takes the `Term End Date - Start Date`. Any partial month will automatically be rounded up to the next month (2 months 1 day will automatically round up to 3 months).
   * Multiple Quotes: If there are multiple quotes, the values from the `Primary Quote` (Quote) will be pushed to the opportunity. If there is not a   primary quote, this values can be manually entered.
   * No Quotes
      * Sales-Assisted- All Types: If there are no quotes, the value will be null and can be manually entered.
      * Web Direct- New Business or Renewal: This value will be hardcoded with `12` months since all web direct opportunities are 1 year in length.
      * Web Direct- Add On Business: This value will take the `End Date` (Subscription) `- Start Date` (opporutnity created date).
5. MRR (Monthly Recurring Revenue) is a value that is captured based on certain scenarios:
   * Single Quote
      * For New Business or Renewals with a single quote, this value will be populated via the `MRR` field on the Quote object.
      * For Add-On Business with a single quote, this value will be populated via `Delta MRR` field on the Quote object.
      * Multiple Quotes: If there are multiple quotes, the values from the `Primary Quote` will be pushed to the opportunity. If there is not a primary quote, this values can be manually entered.
   * No Quotes
      * Sales-Assisted: If there are no quotes, the value will be null and can be manually entered.
      * Web Direct- New Business or Renewal: MRR will be the `Amount/12`
6. Start Date varies by type and is captured based on certain scenarios:
   * Single Quote: for all types of business with a single quote, this field will be populated via the `Start Date` field. For new business or renewal, this will be the date that the new or renewal subsription starts, respectively. For Add-On Business, this will be the start date of the amendment.
   * Multiple Quotes: If there are multiple quotes, the values from the `Primary Quote` will be pushed to the opportunity. If there is not a primary quote, this values can be manually entered.
   * No Quotes
      * Sales-Assisted: If there are no quotes, the value will be null and can be manually entered.
      * Web Direct- New Business or Add-On Business: For new web direct purchases, the `Start Date` will be the `Created Date` of the opportunity
      * Web Direct- Renewal: For web direct renewals, the `Start Date` will be the `Term End Date` + 1 of the subscription.
6. End Date is a value that is captured based on certain scenarios:
   * Single Quote
      * For New Business or Renewals, this field will be populated via a workflow that will take the End Date from the `Start Date + Opportunity Term`.
      * For Add-On Business, this field will be populated via the `Term End Date` on the Quote object.
   * Multiple Quotes: If there are multiple quotes, the values from the `Primary Quote` will be pushed to the opportunity. If there is not a primary quote, this values can be manually entered.
   * No Quotes
      * Sales-Assisted: If there are no quotes, the value will be null and can be manually entered.
      * Web Direct- New Business and Renewals: Since all web direct purchases are for 12 months, the End Date will be calculated taking the `Start Date + 12 months`
      * Web Direct- Add-On Business: End Date will be taken from the `Term End Date` on the Subscription object.

If a quote is sent to Zuora, the values from the Quote will overwrite any values on the opportunity, regardless of whether the quote is primary or not. If there are discrepancies between any of the values, such as the MRR, Amount, or Start and End Dates, please contact Finance or Sales Operations to resolve. We will either need to make amendments to the subscription or manually override the value in the Opportunity.

#### How to Handle Duplicate Accounts and Opportunities from Web Direct Purchases

In some cases, a prospect or customer that is currently engaged with an AE or AM on an opportunity might be proactive and sign up online via the web portal. If this occurs, then a duplicate Account, Opportunity, and Contact may be created. In the event that a duplicate opportunity is created, please do the following to resolve. Unfortunately there is no current method of merging opportunities so the work is manual at this time:

1. Go to the web direct opportunity and add a Chatter note to Francis Aquino. To message someone directly, add the `@` sign before typing their name.
1. Add this note in Chatter: "Here is the duplicate web direct for the following opportunity <URL of original opportunity>".
1. Sales Ops will mark the web direct opportunity as `9-Duplicate` while marking the original opportunity as `6-Closed Won`. This will ensure that historical creation dates, lead sources, and other information is maintained for analysis.
1. If a duplicate account is created, Sales Ops will merge the two accounts together and change the CRM ID in Zuora from the new account to the old account.

If the prospect is still a Lead record that has not converted into an Account, please complete the following steps:

1. Go to the Lead record and convert it into an account, contact, and opportunity as you normally would any qualified opportunity.
1. Then follow Steps 1-4 in the previous section.

#### Closing Deals for GitLab.com

* The Sold To contact should match the email used on the GL.com account.
* The customer then needs to reset their password on the subscription portal.
* Once they are logged in the portal, the customer can click on the `Update` button of the subscription card.
* The customer needs to pick one of their Groups that were fetched from GL.com and proceed to update the subscription.
* Finally, the customer should then be able to see the right plan for their group on their GL.com account.

#### Closing Deals for GitHost

 * Confirm with client the best contact for installation, the type of plan, and how many EE users will be needed, then create quote.
 * When the quote is created, the system will automatically add the GitHost Subscription Terms to the quote in addition to the GitLab EE Subscription Terms.
 * Once quote is singed, send to zbilling
 * Once sent to zbilling there will be an automated email that goes out to support@gitlab.com alerting the support team to contact the client to connect their GitLab EE license key with the GitHost instance
 * A second email will be sent to the client with the GitLab EE license key and the process will be complete

#### Closing Deals for Educational Institutions receiving educational pricing

 * The customer should purchase a license as normal through Zuora, except that the number of users purchased should only include non-student users (as described in [Educational Pricing](https://about.gitlab.com/license-faq/)).
 * After the customer purchases the license, the account executive then manually creates and sends a license that includes the total number of users, where `total # of users = students + non-students`.

#### View and download invoices in Salesforce

 As soon as an invoice is generated, the sales rep can view and download it as a PDF in Salesforce. Scroll to the bottom within the Salesforce-Account and click on the invoice number under "Invoices". Then on the bottom of the invoice view, click "Invoice PDF".

 A paid invoice will have a zeroed Balance and positive Payment Amount.

### Returning Customer Creation Process(Upgrade/Renewals/Cancellations)

1. Create an Opportunity for an [Add-on or Renewal](https://about.gitlab.com/handbook/sales/#opportunity-types).  If a cancellation, click on Opportunity they want to cancel.
1. Click on New Quote button within the opportunity.
1. Since this is a returning customer, sales rep will see a screen showing the current subscription.
1. This screen determines that this customer is already established in Zuora and will allow the sales rep to perform on the 4 actions –

#### Updating Subscription for the account

1. Amend existing subscription for this billing account
2. Renew existing subscription for this billing account
3. Cancel existing subscription for this billing account

##### New Subscription for the account

1. This will allow the Sales rep to create a [new subscription](https://about.gitlab.com/handbook/sales/#opportunity-types) for the existing billing account.
1. Clicking on Next will take the sales rep thru the same Quoting flow that was seen the new Quote creation process

##### Amend existing subscription for the billing account

1. This process is used to perform any [upgrade to a subscription plan or add-on of additional seats t same subscription](https://about.gitlab.com/handbook/sales/#opportunity-types)on an existing subscription.
1. Choosing “Amend existing subscription for billing account”, will allow sales rep to perform amendment to an existing subscription.(Upgrades)
1. Clicking on the this radio button will list all subscriptions that are tied to the customer
1. Click to choose the subscription for performing an Amendment and hit on Next button

##### Cancel existing subscription for the billing account

1. This process is used to cancel an existing subscription. (Note that if a customer purchases seats for the wrong billing account, please see the next section.)
1. Click on Opportunity to cancel.
1. Click on New Quote
1. Since this is a returning customer, sales rep will see a screen showing the current subscription.
1. Choosing “Cancel existing subscription for billing account”,
1. Clicking on the this radio button will list all subscriptions that are tied to the customer
1. Click the subscription for performing a cancellation and hit on Next button
1. Select cancellation date and click Next Button.

##### Cancel an erroneous subscription for the billing account

1. This process is used to cancel an erroneous subscription.
1. For example, a customer may purchase additional seats or products via the web portal which was originally intended as on add-on to an existing subscription.
1. Provide Finance or Sales Operations with the erroneously created Zuora Subscription ID, invoice number and the correct Zuora Subscription ID.
1. Finance will cancel the subscription and either refund the invoice (if a credit card purchase) or cancel the invoice (if check or other payment method).
1. Finance will then amend the correct Zuora Subscription ID and will either charge the card on file or send the invoice via email.

#### Zuora Supporting 4 types of Amendments –

1. Terms and Conditions amendment – The sales rep will be able to change the terms and conditions of an existing subscription;
1. Remove product Amendment – The sales rep will be be able to perform a Remove a product Amendment;
 * In this case, sales rep will have to Set the Start date (Contract effective date in Zuora terms) when the remove product amendment should happen
 * Click on Next
 * This will take them to the product selector page and displays the original product that was purchased within the subscription
 * Sales rep can now remove the product
1. Add product Amendment – Sales rep can add a new product from the product selector page
1. Update Product Amendment – Sales rep can update the existing product of the existing selected product
 * Note: Do not change the Terms and conditions unless you are performing a terms and conditions amendment(except for start date).
       Let's take an example - Let's say a customer once to add more seats to their license.  
       1. Set the start date
       1. Change the quantity field to reflect the new total number of seats
         * Hit on Save

Once on the Quote Summary, will click on generate PDF to generate a Quote PDF
Send it to the customer - only if there is no record of a signed quote. If customer purchased online, they agreed to our terms and condition, so no need to have them sign a quote.  It is ideal though.
Upon Sign-off, or existing signed quote, click on the Send to Z-billing button to send the Quote over to Zuora

##### Renew existing subscription for the billing account

1. This process is used to perform Renewal on an existing subscription; this is only created if the AUTO RENEW Flag is set to “NO” for a subscription initially.
1. Choosing “Renew existing subscription for billing account”, will allow sales rep to perform Renewal to an existing subscription;
1. Clicking on the this radio button will list all subscriptions that are tied to the customer
1. Clicking on next will take the sales to the Create Renewal Quote page
1. Sales rep will select the renewal Quote Template from the list
1. Enter the Renewal term in months
1. Will hit on Next
1. Skip the product selector page, unless want to update the QTY or want to add a new product
 * if they are adding more seats, change the quantity field to reflect the new total number of seats
1. Once on the Quote Summary, will click on generate PDF to generate a Quote PDF
1. Send it to the customer, via Sertifi button within Zquote screen
1. Upon Sign-off will click on the Send to Z-billing button to send the Quote over to Zuora
1. Close Won the opportunity

##### Renew existing subscription with a "true-up" for the billing account

1. This process is used to perform a Renewal on an existing subscription and to add a one time charge for true up; this is only created if the AUTO RENEW Flag is set to “NO” for a subscription initially.
1. Choosing “Renew existing subscription for billing account”, will allow sales rep to perform Renewal to an existing subscription;
1. Clicking on the this radio button will list all subscriptions that are tied to the customer
1. Clicking on next will take the sales to the Create Renewal Quote page
1. Sales rep will select the renewal Quote Template from the list
1. Enter the Renewal term in months
1. Will hit on Next
1. On the product selector page, add the true up product
 * enter the number of seats they will true-up.  Add in the price of the seat, 100% of price they paid.
1. Next, update their current subscription to reflect the new total number of seats they will be renewing for which will be equal or greater than the amount they had with their subscription plus the true up amount.
1. Once on the Quote Summary, will click on generate PDF to generate a Quote PDF
1. Send it to the customer, via Sertifi button within Zquote screen
1. Upon Sign-off will click on the Send to Z-billing button to send the Quote over to Zuora
1. Close Won the opportunity

#### Subscription Types and Opportunity Type Validation Rules

The following Subscription Types (New, Renewal, Amendment) must match the corresponding Opportunity Types (New, Renewal, Add On) in Salesforce:

* New Subscription = New Business
* Amendment = Add On Business
* Renewal = Renewal Business

If you attempt to create a `New Subscription` when the Opportunity Type is `Add On Business` or `Renewal`,  Salesforce will reject your request unless you obtain approval from Finance or Sales Operations. A major reason for this is because of churn considerations, as creating a new subscription instead of amending the existing subscription will cause this subscription to count towards our churned metrics. With approval, Sales Ops and Finance are made aware of the special circumstance and can make any adjustments needed to ensure the transaction will not count as churn.

A list of instances when the Subscription Type may not necessarily match the Opportunity Type include:
* **Reset of customer terms.** If the customer wishes to reset their terms in the middle of their term (for example, they want to upgrade, but want to reset their term for another 12 months), you will need create a new subecription. In this case, the Subscription Type will be 'New' while the Opportunity Type will be 'Renewal'.
* **Co-termination of multiple subscriptions.** If the customer has multiple groups and wishes to consolidate their subscriptions, an "Amendment" may be created against a "Renewal Business" opportunity.
* **Splitting out a single subscription**. Conversely, there may be times when a customer needs to split  their single subscription into multiple subscriptions. When this occurs, the Subscription Type and Opportunity Type will be 'Renewal'.

If your opportunity falls into any one of these three categories, on the opportunity object and Chatter mention @francis or @wilson, and we will create the quote for you.

##### Tracking Churned Subscriptions and Opportunities

In the event that a customer downgrades, terminates, or does not renew their subscription, Sales Operations and Finance will process the churn information in the opportunity record. Churned opportunities will be identified by having a negative Incremental Annual Contract Value (IACV). The following information will be captured:

* Churn Type will capture the nature of the downgrade/non-renewal:
  * "Price Per User Downgrade" is used when a customer renews with the same product, but the per unit price is discounted.
  * "Edition Downgrade" is used when a customer downgrades to a lower paid package (from EEP to EES). If a customer is downgrading from any paid packages to the Community Edition, this would be considered a Non-Renewal as they are no longer a paying customer.
  * "Seat Reduction" is used when a customer reduces the number of seats from the previous year at renewal.
  * "Non-Renewal" is used when a customer requests to cancel their subscription upon renewal, either to the Community Edition or to a competitor.
* Churn ACV: The annual contract value that was lost.
* Churn Month: The month that the churn occurred. Most of the time, this will be the close date. However, a churn may be retroactive and occur in a prior month.
* Churn Notes: Open text field to capture any additional notes regarding the churn. If the customer has provided specific reasons for churn, the details will be entered in this field.

The following events will trigger an email to Sales Operations and Finance for tracking churn:

* An opportunity has been submitted for approval with a negative amount. This signifies either a credit or refund of a subscription was requested by a customer and processed by an Account Executive or Account Manager.
* A renewal opportunity has been won, but has a negative IACV. This signfies that the customer has downgraded. Sales Operations and Finance will reach out to the opportunity owner to understand the nature of the churn.
* A renewal opportunity has been lost. This signifies that the customer did not renew their subscription. Sales Operations and Finance will reach out to the opportunity owner to understand why the customer did not wish to renew.

### Creating a Quote for an Authorized Reseller Partner

A reseller quote has a few different things than a regular quote:

* Before you generate a quote for an authorized reseller, make sure to go to the reseller's account record in Salesforce. In the Account Detail section, enter the Payment Term (30, 60, etc) that has been agreed upon between GitLab and the reseller. Make sure to only enter the numeric value and not "Net 30" or "Net 60". This value will appear in the Quote document in the Terms section.
* Quote Name Field:  append “via reseller name” to the Quote name (i.e.: “Quote for Federal Reserve via ReleaseTEAM”)
* Quote Template:  Needs to be a reseller template, such as "Reseller New Quote" or "Reseller Amendment Quote".  Since resellers cannot accept terms for their customers, the reseller template contains different language around acceptance.
* Sold To Contact and Bill To Contact fields both need to be a person at the end customer.  This is who will accept the EULA.
* Invoice Owner Field:  This needs to be the resellers account.  If you do not see the reseller listed, then you need to send the SFDC URL of the reseller’s billing contact to finance and for an Invoice Owner record to be created.
* Invoice Owner Contact: This is the Bill To Contact for the reseller. This is important as the Bill To Contact's account record will populate the GitLab entity, and all information related to that GitLab entity (Entity Name, Entity Contact Info, Entity Banking Information, and Entity Beneficiary Information).
* Click Through EULA required: Set this to Yes.  This will cause a URL to be sent to the customer where they agree to our Terms and Conditions before getting their license key.  This is important as a reseller cannot agree to terms on behalf of the end user.  Alternatively, the reseller could obtain a physical signature and send it to you.
* Discount: Authorized resellers all have pre-defined discounts depending upon the market they serve and the services they provide to GitLab.  GitHost is never discounted as our margin after paying Digital Ocean is very small.  We do not give discounts to fulfillment houses like SHI, Insights, or other resellers that are not authorized resellers.  Reseller discounts can be found on the first page of the [Resellers List](https://docs.google.com/spreadsheets/d/1tQjPMRUuzsDR4mNj74aY-W8jBQH4u9h7PpEsw088Zx0/edit#gid=1395032632)
When in doubt please consult the reseller team.

### Using Customer Form Agreements
{: #CustomerFormAgreements}

Our experience shows that using a prospect's form agreement is expensive and, more importantly, time consuming.  Deals in which we use the customer agreement take on average 60 days longer to close than if completing using our standard subscription agreement with changes as requested by customer counsel.  The arguments in favor of using our agreement are as follows:

1. Our agreement is an annual subscription agreement with a true-up whereas customer form agreements typically are based on paid up licenses.
1. We are an open source company and our agreement provides licenses for both the CE version of the product and the EE version as well as dealing with contributions of code from our customer.
1. We have non-standard but customer favorable warranty and acceptance provisions.
1. We are very accommodating of customer requested changes to our form agreement which is why we can close deals quickly.

Despite the overwhelming arguments in favor of using the GitLab form some prospects insist on using their form agreement.  GitLab will accommodate such requests with the following assumptions:

1. GitLab must have been selected as the solution of choice by the customer.
1. The deal must be in excess of $15,000.
1. The primary decision maker must indicate their willingness to facilitate the internal process in a manner that brings issues to closure within 30 days of negotiations.  The decision maker must also acknowledge that they understand that the form agreement may require significant revisions based on what is described in the above section.
Items 1 and 3 above must be acknowledged in writing prior to proceeding with the contract markup.

### Using Cases in Salesforce

A Case is a question or feedback from a prospect or customer. While there are many ways that a customer can reach out to GitLab, the following are the only email addresses that will create a Case in Salesforce. If the Case submitter is an existing Contact in Salesforce, the case will be associated to both the Contact and Account record, and will be assigned to the Contact owner. If the Case submitter is not an existing Contact in Salesforce, please see below who the case will be assigned to.

* Sales@: this is an inquiry from a prospect requesting information on GitLab's services. These inquiries will create cases in Salesforce in the [BDR Queue](https://na34.salesforce.com/500?fcf=00B610000042ioq) and will be handled by the BDR Team. The inquiries are routed to the appropriate team per the rules outlined below. This email is in process of being deprecated.    
* Renewals@: this is an inquiry from an existing customer. These inquiries should be handled by various team members, depending on the nature of the inquiry.  Cases not associated to an existing Account will be assigned to the Account Management queue.

##### New Business Inquiries

All new business inquiries will be handled by the BDR team. However, if an inquiry comes in for an account owned by an AE - Mid-Market or above or SAL, these will be treated similar to `Contact Us` requests and routed to the named account owner. Before the case is reassigned, the BDR will search Salesforce for a matching record:  
  * If a matching record exists, both the Case and record will be reassigned to the SAL/AE - Mid-Market or above. The new owner will be notified upon reassignment.
  * If a record does not exist, the Case will be reassigned to the MMAE+/SAL and the new owner will be responsible for creating a LEAD record.


##### Existing Customer Inquiries
Existing customer inquiries should be routed based to the appropriate team based on the inquiry:

For questions on **additional seats**, **additional products**, **product upgrades**, **true-ups**, **renewals**, **subcription portal password resets**, or **cancellations**:
* Strategic/Large customers will be reassigned to the Account Owner and the TAM will be notified.
* Mid-Market customers will be reassigned the Account Manager by Region.
* SMB customers will be reassigned to SMB Advocate.
* For all partner-owned accounts, the inquiry should be routed to the Channel Manager, who is listed on the Account record under the GitLab Team section.
* If there is no account associated to the Case, it should be reassigned to the Account Management queue.

For **refunds**, **invoice requests**, **changes to billing information (email address, company name, etc)**, **customer web portal purchasing assistance** regardless of Sales Segment:
1. Respond with the "Forward to AR" template in Outreach.
1. Reassign the case to GitLab AP user in Salesforce.

For all **technical questions** regardless of Sales Segment:
1. Respond with the "Forward to Support" template in Outreach
1. Enter "Forwarded to Support" in the `Case Reason` field
1. Close the Case as this case will be managed by the support team in Zendesk.

For re-issuing license keys:
1. Forward the email to Oswaldo F. or Ruben D.
1. Enter "Existing Client Question" in the `Case Reason` field
1. Close the Case

For adding student users to an existing license key:
1. Request proof of purchase from educational institution for staff users, which will either be an invoice or the signed order form
1. Open the license app and duplicate the license adding the amount of extra seats for students
1. The new license is sent automatically, but the best practice is to attach license to a follow-up explainer email. (Also clarify that they are not entitled to Gitlab technical support)    

 
##### Purchase Order (PO) Fulfillment from a Reseller

In some cases, a reseller may send a purchase order for order fulfillment. When this happens, the following steps will be taken by the BDR:
1. Search Salesforce for the related account and opportunity. Once found, the email will be forwarded to the opportunity owner.
1. Reassign the Case to the Opportunity Owner.
1. Go to the Type field and select `PO Delivery`.
1. Go to the Status field and select `Closed`.
1. Click Save.


#### Accessing Cases

The primary ways to access Cases in Salesforce:

1. Cases display in a Cases related list on the Account, Contact, or Opportunity record. This is best when you are looking for Cases assigned to a specific Account or Contact.
2. You can go to the Cases tab in Salesforce, then select My Open Cases (or any other list view available). This is best when you are looking for Cases either assigned to you or your group. It is a good practice to review cases assigned to you at least a few times a day.
3. You can use the Console tab in Salesforce (if unavailable, it can be added by clicking on + to see All Tabs, and then selecting `Customize My Tabs`). This view allows you to view the Cases List and Content in the same window.

#### Working Cases

1. You may be assigned a case that is not associated to a Contact or Account. This happens because the person who submitted the case is not an existing record in the system.
1. When this occurs, a best practice is to create the contact, then associate the Case to that contact.
1. Also, it is a best practice to associate the case to the Account, so that when you, or another team member access the account, the case will be visible to all team members.

#### To work with Email-to-Case or On-Demand Email-to-Case emails

* Click Send An Email to send an email to a contact, another user, or any other email address.
* Click Reply to respond to an email. The email response automatically includes the email body as received from the customer. Enter your response and click Send.
* Click To All to respond to all participants on an email thread.
* The email address of the contact who created the case automatically appears in the To field, as long as the creator is an existing contact. To add more recipients, click Lookup icon to look up an address, or type or paste email addresses or names in the To field.
* When you enter an email address or name that matches one contact or user, the address appears as a button with the person’s name.
* To add several addresses at once, copy and paste them separated by spaces or commas. These email addresses appear as buttons, and, if they’re associated with a contact or user, show the contact’s name.
* When you enter an email address that matches multiple contacts or users, the address appears as a button. Clicking the button brings up a list of people associated with the email address so you can choose the one you want to associate with the message.
* If you need to copy other people on the message, click Add Cc or Add Bcc.
* By default, the email subject is the name of the case it’s related to. You can edit the subject if you need to.

* Click the subject of the email to view the email. From the email, you can reply to the sender, reply to everyone, forward the email, or delete it.
* While viewing an email, you can display a list of all the emails associated with the case by clicking Email Message List, and you can navigate to the case's other emails by clicking Next or Previous.
* While viewing an email, click Forward to forward it. The email automatically includes the email body as received from the customer. Optionally, enter text and click Send.

#### Closing a Case

* Before closing a case, make sure that the account is associated with the case
* Choose the case reason before you close

### Process after you close a Premium Support Subscription

Once you close a deal that includes Premium Support, you need to:

1. On the account object in Salesforce, check the **Support Level** in the _GitLab Subscription Information_ section, so that it carries through to Zuora and Zendesk.
2. Notify the Customer Success team there is a new Premium support customer, by creating an issue on the [Customer Success](https://gitlab.com/groups/gitlab-com/customer-success/-/issues) project using the _Premium support onboarding_ template. Be sure to include all parts of the issue:
   - Name of organization
   - Domain name, and optionally named individuals that are most likely to submit support tickets
   - Link to the SalesForce record.
   - Mark the issue confidential!
